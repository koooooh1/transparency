from django.urls import path
from .views import *


# define urls of sub-sites
urlpatterns = [
    # home
    path('', ArticleSummaryHome.as_view(), name="articlesummaries_home"),

    # article summary
    path('create/', ArticleSummaryCreateView.as_view(), name="articlesummaries_create"),
    #path('readbibtex/', ArticleSummaryReadBibtexView.as_view(), name="articlesummaries_readbibtex"),
    path('<slug:slug>/delete/', ArticleSummaryDeleteView.as_view(), name="articlesummaries_delete"),
    path('<slug:slug>/update/', ArticleSummaryUpdateView.as_view(), name="articlesummaries_update"),

    # general
    path('list/', ArticleSummaryListView.as_view(), name="articlesummaries_list"),
    path('<slug:slug>/', ArticleSummaryDetailView.as_view(), name="articlesummaries_detail"),
]
