from django.db import models
from django_extensions.db.fields import AutoSlugField
from django.core.validators import MaxValueValidator, MinValueValidator

from main.globalvariables import AREA_CHOICES, FORM_MIN_YEAR, FORM_MAX_YEAR, FORM_MAX_IMAGE_SIZE
from main.globalclasses import FileSizeValidator
from .apps import ArticlesummariesConfig


# model for the article summaries
class ArticleSummary(models.Model):
    # initialise fields
    title = models.CharField(max_length=1024)
    authors = models.CharField(max_length=1024)
    pub_year  = models.PositiveIntegerField(validators=[MinValueValidator(FORM_MIN_YEAR), MaxValueValidator(FORM_MAX_YEAR)])
    doi = models.CharField(max_length=1024, blank=True)  # add custom validator sometime
    jour_conf = models.CharField(max_length=1024, blank=True)

    area1 = models.CharField(max_length=1024, choices=AREA_CHOICES)
    area2 = models.CharField(max_length=1024, choices=AREA_CHOICES, blank=True)
    area3 = models.CharField(max_length=1024, choices=AREA_CHOICES, blank=True)

    abstract = models.CharField(max_length=1500)
    summary = models.CharField(max_length=7000)

    image = models.ImageField(upload_to='images/'+ArticlesummariesConfig.name, null=True, blank=True, validators=[FileSizeValidator(FORM_MAX_IMAGE_SIZE)])
    caption = models.CharField(max_length=1024, blank=True)

    slug = AutoSlugField(populate_from=['title'])
    # user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True)
