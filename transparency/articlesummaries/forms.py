from .models import ArticleSummary
from django import forms
from main.globalvariables import AREA_CHOICES


# form for the article summaries
class ArticleSummaryForm(forms.ModelForm):
    class Meta:
        model = ArticleSummary
        exclude = ('slug',) # add user here sometime

        # specify field properties
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-object'}),
            'authors': forms.TextInput(attrs={'class': 'form-object'}),
            'pub_year': forms.NumberInput(attrs={'class': 'form-object'}),
            'doi': forms.TextInput(attrs={'class': 'form-object'}),
            'jour_conf': forms.TextInput(attrs={'class': 'form-object'}),

            'area1': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(1, this.value)'}),
            'area2': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(2, this.value)'}),
            'area3': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(3, this.value)'}),

            'abstract': forms.Textarea(attrs={'class': 'form-object', 'placeholder': 'Limited to 1500 characters', 'rows': 6}),
            'summary': forms.Textarea(attrs={'class': 'form-object', 'placeholder': 'Limited to 7000 characters', 'rows': 10}), 

            'image': forms.FileInput(attrs={'class': 'form-file-upload'}),
            'caption': forms.TextInput(attrs={'class': 'form-object', 'placeholder': 'Optional caption'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # specify individual labels
        self.fields['title'].label = 'Title * '
        self.fields['authors'].label = 'Authors * '
        self.fields['pub_year'].label = 'Year of Publication * '
        self.fields['doi'].label = 'DOI'
        self.fields['jour_conf'].label = 'Journal / Conference'

        self.fields['area1'].label = 'Choose an area * '
        self.fields['area2'].label = 'Choose a second area'
        self.fields['area3'].label = 'Choose a third area'
        
        self.fields['abstract'].label = 'Abstract * '
        self.fields['summary'].label = 'Summary * '

        self.fields['caption'].label = ''

    def clean(self):
        cleaned_data = super().clean()

        # delete paragraphs from copying from a PDF file
        cleaned_data['abstract'] = cleaned_data['abstract'].replace('\r', ' ').replace('\n', ' ')
 

# form to read a bibtex file
class ArticleSummaryReadBibtex(forms.Form):
    file_field = forms.FileField()