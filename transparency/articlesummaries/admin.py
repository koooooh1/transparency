from django.contrib import admin
from .models import ArticleSummary

# Register your models here.
class ArticleSummaryAdmin(admin.ModelAdmin):
    pass
admin.site.register(ArticleSummary, ArticleSummaryAdmin)