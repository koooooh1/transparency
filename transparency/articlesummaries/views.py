# import view classes
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView, FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

# import further django classes
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q
# import app-specific model, forms and appname
from .models import ArticleSummary
from .forms import ArticleSummaryForm, ArticleSummaryReadBibtex
from .apps import ArticlesummariesConfig

# import global variables
from main.globalvariables import AREA_CHOICES, CREATE_BUTTON_TEXTS, NUMBER_OF_ELEMTS_ON_PAGE, VIEW_FULL_TEXTS, DELETE_TEXTS

# import for readbibtex-view
from pybtex.database.input import bibtex
from urllib import parse



# app home page
class ArticleSummaryHome(TemplateView):
    template_name = 'app_templates/app_home.html'

    # declare strings on homepage
    text_search = 'In this section you can find and read summaries of articles and papers and their related abstracts. \
        The summaries are ordered by area. \
        Either select one of the areas listed below in order to get an overview of the area-specific  \
        summaries.'# or find a certain summary using the keyword-based search bar.'
    #text_add = 'You can either upload a BibTex-File to enter your articles data or enter this data manually.'
    text_add = 'You can create a new article summary by clicking the button button below.'
    
    extra_context={
        'appname': ArticlesummariesConfig.name,
        'areas': AREA_CHOICES[1:],
        'header_search': 'Search for Brief Article Summaries',
        'text_search': text_search,
        'header_add': 'Add a Brief Article Summary',
        'text_add': text_add,
        'text_button': CREATE_BUTTON_TEXTS[ArticlesummariesConfig.name]
    }


# create an article summary
class ArticleSummaryCreateView(CreateView):
    model = ArticleSummary
    form_class = ArticleSummaryForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': ArticlesummariesConfig.name
    }

    def get_success_url(self):
        # redirect to detail view of newly created article summary
        return reverse('articlesummaries_detail', kwargs={'slug': self.object.slug})
    
    # read url-query for content of bibtex-import
    #def get_initial(self):
    #    initial = super().get_initial()
    #    query_dict = parse.parse_qs(parse.urlsplit(self.request.get_full_path()).query)
    #    for key in query_dict:
    #        initial[key] = query_dict[key][0]
    #    return initial


# view to read a bibtex file for creating an article summary 
class ArticleSummaryReadBibtexView(FormView):
    form_class = ArticleSummaryReadBibtex
    template_name = 'app_templates/articlesummaries_readbibtex.html'
    extra_context = {
        'appname': ArticlesummariesConfig.name
    }

    def post(self, request, *args, **kwargs): 
        # called when form is submitted

        self.object = []

        # get uploaded file from form
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        bibtex_file = request.FILES['file_field'].read()

        if form.is_valid():
            # read bibtex file and get the necessary information
            # but this is incomplete

            bibtex_file = str(bibtex_file).replace('\\n', '').replace('\\r', '')
            bibtex_data = bibtex.Parser().parse_string(bibtex_file)
            bibtex_entries = list(bibtex_data.entries.keys())

            if len(bibtex_entries) > 1:
                messages.info(request, 'Only upload BibTex-Files with just one entry!')
                return self.form_invalid(form)
            else:
                entryname = bibtex_entries[0]

                self.urlquery = 'title=' + bibtex_data.entries[entryname].fields['title'] \
                    + '&authors=' + str(bibtex_data.entries[entryname].persons['author'][0]) \
                    #+ "&pub_date=2015-01-01"

                return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        # redirect to create an article summary to enter missing fields
        return ("%s?" + self.urlquery) % reverse('articlesummaries_create')
    

# delete an article summary
class ArticleSummaryDeleteView(DeleteView):
    model = ArticleSummary
    template_name = 'app_templates/delete.html'
    extra_context = {
        'appname': ArticlesummariesConfig.name,
        'text_delete': DELETE_TEXTS[ArticlesummariesConfig.name]
    }

    def get_success_url(self):
        # redirect to listing of all article summaries
        return reverse('articlesummaries_list')


# update an article summary
class ArticleSummaryUpdateView(UpdateView):
    model = ArticleSummary
    form_class = ArticleSummaryForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': ArticlesummariesConfig.name
    }

    def get_success_url(self):
        # redirect to detail view of updated article summary
        return reverse('articlesummaries_detail', kwargs={'slug': self.object.slug})


# detail view for an article summary
class ArticleSummaryDetailView(DetailView):
    model = ArticleSummary
    template_name = 'app_templates/detail.html'
    slug_field = 'slug'
    
    def get_context_data(self,**kwargs):
        extra_context = super(ArticleSummaryDetailView,self).get_context_data(**kwargs)
        extra_context['appname'] = ArticlesummariesConfig.name
        return extra_context


# listing of all article summaries
class ArticleSummaryListView(ListView):
    model = ArticleSummary
    template_name = 'app_templates/list.html'
    ordering = ['-pub_year']
    paginate_by = NUMBER_OF_ELEMTS_ON_PAGE
    extra_context = {
        'appname': ArticlesummariesConfig.name,
        'areas': AREA_CHOICES[1:],
        'text_button': CREATE_BUTTON_TEXTS[ArticlesummariesConfig.name],
        'text_viewfull': VIEW_FULL_TEXTS[ArticlesummariesConfig.name],
    }

    def get_context_data(self, **kwargs):
        # add extra variables for the template
        data = super().get_context_data(**kwargs)

        # get selected area from url query
        data['url_area'] = self.request.GET.get('area')

        # get total number of article summaries for paginator
        data['num_entries'] = self.get_queryset().count()
        
        data['num_all_entries'] = super().get_queryset().count()
        
        # get number of post with areas
        num_entries_area = []
        for area in AREA_CHOICES:
            if area[0] != '':
                mytuple = (area[0], area[1], super().get_queryset().filter(Q(area1=area[0]) | Q(area2=area[0]) | Q(area3=area[0])).count())
                num_entries_area.append(mytuple)
        data['num_entries_area'] = num_entries_area
        
        return data

    def get_queryset(self):
        # get selected area from url query
        area = self.request.GET.get('area')

        # get article summaries database
        q = super().get_queryset()

        # only display article summaries with selected area
        if area == 'all':
            return q
        else:
            return q.filter(area1=area) | q.filter(area2=area) | q.filter(area3=area)
        
    def dispatch(self, *args, **kwargs):
        if not self.request.GET.get('area'):
            # redirect to area=all if no area variable is in url query
            return HttpResponseRedirect(reverse('articlesummaries_list') + "?area=all")
        else:
            return super().dispatch(*args, **kwargs)
 