from django.apps import AppConfig


class ArticlesummariesConfig(AppConfig):
    name = 'articlesummaries'
