from django.urls import path
from .views import *


# define urls of sub-sites
urlpatterns = [
    # home
    path('', ProjectSummaryHome.as_view(), name="projectsummaries_home"),
    
    # project summary
    path('create/', ProjectSummaryCreateView.as_view(), name="projectsummaries_create"),
    path('<slug:slug>/delete/', ProjectSummaryDeleteView.as_view(), name="projectsummaries_delete"),
    path('<slug:slug>/update/', ProjectSummaryUpdateView.as_view(), name="projectsummaries_update"),

    # general
    path('list/', ProjectSummaryListView.as_view(), name='projectsummaries_list'),
    path('<slug:slug>/', ProjectSummaryDetailView.as_view(), name="projectsummaries_detail"),
]
