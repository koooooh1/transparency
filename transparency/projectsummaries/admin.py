from django.contrib import admin
from .models import ProjectSummary

# Register your models here.
class ProjectSummaryAdmin(admin.ModelAdmin):
    pass
admin.site.register(ProjectSummary, ProjectSummaryAdmin)