from django.apps import AppConfig


class ProjectsummariesConfig(AppConfig):
    name = 'projectsummaries'
