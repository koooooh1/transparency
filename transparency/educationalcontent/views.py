# import view classes
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

# import further django classes
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.db.models import Q


# import app-specific models, forms and appname
from .models import EducationalContentCourse, EducationalContentLecture
from .forms import EducationalContentForm, EducationalContentLectureForm
from .apps import EducationalcontentConfig

# import global variables
from main.globalvariables import AREA_CHOICES, CREATE_BUTTON_TEXTS, NUMBER_OF_ELEMTS_ON_PAGE, VIEW_FULL_TEXTS, DELETE_TEXTS



# app home page
class EducationalContentHome(TemplateView):
    template_name = 'app_templates/app_home.html'

    # declare strings on homepage
    text_search = 'In this section you can find and read summaries of articles and papers and their related abstracts. \
        The summaries are ordered by area. \
        Either select one of the areas listed below in order to get an overview of the area-specific  \
        summaries.'# or find a certain summary using the keyword-based search bar.'
    #text_add = 'You can either upload a BibTex-File to enter your articles data or enter this data manually.'
    text_add = 'You can upload your own educational content by clicking the button below.'
    
    extra_context={
        'appname': EducationalcontentConfig.name,
        'areas': AREA_CHOICES[1:],
        'header_search': 'Search for Educational Content',
        'text_search': text_search,
        'header_add': 'Add Educational Content',
        'text_add': text_add,
        'text_button': CREATE_BUTTON_TEXTS[EducationalcontentConfig.name]
    }


# create a course
class EducationalContentCreateCourseView(CreateView):
    model = EducationalContentCourse
    form_class = EducationalContentForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': EducationalcontentConfig.name
    }

    def get_success_url(self):
        # redirect to detail view of newly created course
        return reverse('educationalcontent_detail', kwargs={'slug': self.object.slug})
    

# delete a course
class EducationalContentDeleteCourseView(DeleteView):
    model = EducationalContentCourse
    template_name = 'app_templates/delete.html'
    extra_context = {
        'appname': EducationalcontentConfig.name,
        'text_delete': DELETE_TEXTS[EducationalcontentConfig.name][0]
    }

    def get_success_url(self):
        # redirect to listing of all courses
        return reverse('educationalcontent_list')


# update a course
class EducationalContentUpdateCourseView(UpdateView):
    model = EducationalContentCourse
    form_class = EducationalContentForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': EducationalcontentConfig.name
    }

    def get_success_url(self):
        # redirect to detail view of updated course
        return reverse('educationalcontent_detail', kwargs={'slug': self.object.slug})
    

# create a lecture
class EducationalContentCreateLectureView(CreateView):
    model = EducationalContentLecture
    form_class = EducationalContentLectureForm
    template_name = 'app_templates/create_foreignKey.html'
    extra_context = {
        'appname': EducationalcontentConfig.name
    }

    def get_success_url(self):
        # redirect to the detail view of course
        return reverse('educationalcontent_detail', kwargs={'slug': self.get_course_slug()})

    def get_initial(self):
        initial = super().get_initial()

        # select course where to add the content from url-slug
        course = EducationalContentCourse.objects.get(slug=self.get_course_slug())

        # set that course as initial to the form
        initial = {'educationalcontent': course}
        return initial
    
    def get_course_slug(self):
        # get course slug from url string 
        return self.request.get_full_path().split('/')[-3]


# delete a lecture
class EducationalContentDeleteLectureView(DeleteView):
    model = EducationalContentLecture
    template_name = 'app_templates/delete.html'
    extra_context = {
        'appname': EducationalcontentConfig.name,
        'text_delete': DELETE_TEXTS[EducationalcontentConfig.name][1]
    }

    def get_object(self):
        # specify that a lecture object should get selected by its id
        return EducationalContentLecture.objects.get(id=self.get_lecture_id())

    def get_success_url(self):
        # redirect to the detail view of course
        return reverse('educationalcontent_detail', kwargs={'slug': self.get_course_slug()})
    
    def get_course_slug(self):
        # get course slug from url string 
        return self.request.get_full_path().split('/')[-4]
    
    def get_lecture_id(self):
        # get lecture id from url string 
        return self.request.get_full_path().split('/')[-3]


# update a lecture
class EducationalContentUpdateLectureView(UpdateView):
    model = EducationalContentLecture
    form_class = EducationalContentLectureForm
    template_name = 'app_templates/create_foreignKey.html'
    extra_context = {
        'appname': EducationalcontentConfig.name
    }

    def get_object(self):
        # specify that a lecture object should get selected by its id
        return EducationalContentLecture.objects.get(id=self.get_lecture_id())

    def get_success_url(self):
        # redirect to the detail view of course
        return reverse('educationalcontent_detail', kwargs={'slug': self.get_course_slug()})
    
    def get_course_slug(self):
        # get course slug from url string 
        return self.request.get_full_path().split('/')[-4]
    
    def get_lecture_id(self):
        # get lecture id from url string 
        return self.request.get_full_path().split('/')[-3]


# detail view for a course
class EducationalContentDetailView(DetailView):
    model = EducationalContentCourse
    template_name = 'app_templates/detail_educationalcontent.html'
    slug_field = 'slug'
    
    def get_context_data(self,**kwargs):
        extra_context = super(EducationalContentDetailView,self).get_context_data(**kwargs)
        extra_context['appname'] = EducationalcontentConfig.name

        # add all lectures 
        extra_context['lectures'] = self.object.lectures.all()

        return extra_context


# listing of all courses
class EducationalContentListView(ListView):
    model = EducationalContentCourse
    template_name = 'app_templates/list.html'
    ordering = ['-pub_year']
    paginate_by = NUMBER_OF_ELEMTS_ON_PAGE
    extra_context = {
        'appname': EducationalcontentConfig.name,
        'areas': AREA_CHOICES[1:],
        'text_button': CREATE_BUTTON_TEXTS[EducationalcontentConfig.name],
        'text_viewfull': VIEW_FULL_TEXTS[EducationalcontentConfig.name]
    }

    def get_context_data(self, **kwargs):
        # add extra variables for the template
        extra_context = super().get_context_data(**kwargs)
        
        # get selected area from url query
        extra_context['url_area'] = self.request.GET.get('area')
        
        # get total number of courses for paginator
        extra_context['num_entries'] = self.get_queryset().count()
        extra_context['num_all_entries'] = super().get_queryset().count()
        # get number of post with areas
        num_entries_area = []
        for area in AREA_CHOICES:
            if area[0] != '':
                mytuple = (area[0], area[1], super().get_queryset().filter(Q(area1=area[0]) | Q(area2=area[0]) | Q(area3=area[0])).count())
                num_entries_area.append(mytuple)
        extra_context['num_entries_area'] = num_entries_area
        
        return extra_context

    def get_queryset(self):
        # get selected area from url query
        area = self.request.GET.get('area')

        # get courses database
        q = super().get_queryset()

        # only display courses with selected area
        if area == 'all':
            return q
        else:
            return q.filter(area1=area) | q.filter(area2=area) | q.filter(area3=area)
        
    def dispatch(self, *args, **kwargs):
        if not self.request.GET.get('area'):
            # redirect to area=all if no area variable is in url query
            return HttpResponseRedirect(reverse('educationalcontent_list') + "?area=all")
        else:
            return super().dispatch(*args, **kwargs)
 