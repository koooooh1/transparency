# Generated by Django 4.2 on 2023-04-26 09:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('educationalcontent', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='educationalcontent',
            old_name='abstract',
            new_name='description',
        ),
        migrations.RemoveField(
            model_name='educationalcontent',
            name='doi',
        ),
        migrations.RemoveField(
            model_name='educationalcontent',
            name='jour_conf',
        ),
        migrations.RemoveField(
            model_name='educationalcontent',
            name='summary',
        ),
        migrations.AddField(
            model_name='educationalcontent',
            name='department',
            field=models.CharField(default='emptydefault', max_length=1024),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='educationalcontent',
            name='difficulty',
            field=models.CharField(choices=[('', 'Select Difficulty...'), ('Easy', 'Easy'), ('Moderate', 'Moderate'), ('Difficult', 'Difficult'), ('VeryDifficult', 'Very Difficult')], default='Easy', max_length=1024),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='educationalcontent',
            name='institutions',
            field=models.CharField(default='emptydefault', max_length=1024),
            preserve_default=False,
        ),
    ]
