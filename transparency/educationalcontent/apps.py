from django.apps import AppConfig


class EducationalcontentConfig(AppConfig):
    name = 'educationalcontent'
