from django.contrib import admin
from .models import EducationalContentCourse, EducationalContentLecture

# Register your models here.
class EducationalContentAdmin(admin.ModelAdmin):
    pass
admin.site.register([EducationalContentCourse, EducationalContentLecture], EducationalContentAdmin)