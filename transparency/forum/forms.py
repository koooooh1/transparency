from django import forms

from .models import ForumPost, ForumAnswer
from main.globalvariables import AREA_CHOICES


# form for the posts
class ForumPostForm(forms.ModelForm):
    class Meta:
        model = ForumPost
        exclude = ('slug', 'date_post', 'date_modified',) # add user here sometime
        
        # specify field properties
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-object'}),
            'authors': forms.TextInput(attrs={'class': 'form-object'}),
            'url': forms.URLInput(attrs={'class': 'form-object'}),

            'area1': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(1, this.value)'}),
            'area2': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(2, this.value)'}),
            'area3': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(3, this.value)'}),

            'text': forms.Textarea(attrs={'class': 'form-object', 'placeholder': 'Limited to 1500 characters', 'rows': 6}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # specify individual labels
        self.fields['title'].label = 'Title * '
        self.fields['authors'].label = 'Author * '
        self.fields['url'].label = 'URL to your competence profile'

        self.fields['area1'].label = 'Choose an area * '
        self.fields['area2'].label = 'Choose a second area'
        self.fields['area3'].label = 'Choose a third area'
        
        self.fields['text'].label = 'Text *'
    
# form for the answers
class ForumAnswerForm(forms.ModelForm):
    class Meta:
        model = ForumAnswer
        exclude = ('date_post', 'date_modified',)
        
        # specify field properties
        widgets = {
            'forumpost': forms.HiddenInput, # hide foreignkey

            'authors': forms.TextInput(attrs={'class': 'form-object'}),
            'url': forms.URLInput(attrs={'class': 'form-object'}), # add validator later
            'text': forms.Textarea(attrs={'class': 'form-object', 'placeholder': 'Limited to 1500 characters', 'rows': 6})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # specify individual labels
        self.fields['forumpost'].label = 'Select post'
        self.fields['authors'].label = 'Your name * '
        self.fields['url'].label = 'URL to your competence profile'
        self.fields['text'].label = 'Text * '
