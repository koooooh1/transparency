from django.db import models
from django_extensions.db.fields import AutoSlugField

from main.globalvariables import AREA_CHOICES


# model for a forum post
class ForumPost(models.Model):
    # initialise fields
    title = models.CharField(max_length=1024)
    authors = models.CharField(max_length=1024)
    url = models.URLField(blank=True) # add validator later
    date_post = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    area1 = models.CharField(max_length=1024, choices=AREA_CHOICES)
    area2 = models.CharField(max_length=1024, choices=AREA_CHOICES, blank=True)
    area3 = models.CharField(max_length=1024, choices=AREA_CHOICES, blank=True)

    text = models.CharField(max_length=1500)

    slug = AutoSlugField(populate_from=['title'])
    # user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True)

    def __str__(self):
        # specify the return text of the str-operator on this class
        return self.title


# model for the answers to a post
class ForumAnswer(models.Model):
    # foreign key to ForumPost
    forumpost = models.ForeignKey(ForumPost, related_name='answers', on_delete=models.CASCADE)

    # initialise fields
    authors = models.CharField(max_length=1024)
    url = models.URLField(blank=True) # add validator later
    date_post  = models.DateTimeField(auto_now_add=True)
    date_modified  = models.DateTimeField(auto_now=True)
    text = models.CharField(max_length=1500)

