# Define global variables for the whole transparency-website
# access them in a file with "from main.globalvariables import ..."


### variables for lists of all existing areas
# for models or select fields use AREA_CHOICES
# for displaying variables use AREA_CHOICES[1:] or AREA STRINGS
AREA_CHOICES = [
    ('', 'Select Area...'),
    ('Bioenergy', 'Bioenergy'),
    ('Buildings', 'Buildings'),
    ('Digitalization', 'Digitalization'),
    ('Hydrogen', 'Hydrogen'),
    ('Industry', 'Industry'),
    ('MobilityandTraffic', 'Mobility and Traffic'),
    ('PowerGrids', 'Power Grids'),
    ('PowerStorage', 'Power Storage'),
    ('RenewableEnergies', 'Renewable Energies'),
    ('ResourceEfficiency', 'Resource Efficiency'),
    ('SectorCoupling', 'Sector Coupling'),
    ('SocietyIssues', 'Society Issues'),
    ('SystemAnalysis', 'System Analysis')
]

# list only with the display-strings
AREA_STRINGS = []
for area in AREA_CHOICES[1:]:
    AREA_STRINGS.append(area[1])


### definition of difficulty levels of courses
DIFFICULTY_LEVELS = [
    ('', 'Select Difficulty...'),
    ('Easy', 'Easy'),
    ('Moderate', 'Moderate'),
    ('Difficult', 'Difficult'),
    ('VeryDifficult', 'Very Difficult')
]


### variables for specific template-texts depending on the appname
CREATE_BUTTON_TEXTS = {
    'articlesummaries': 'New Article Summary',
    'projectsummaries': 'New Project Summary',
    'educationalcontent': 'Create New Course',
    'forum': 'Create New Post'
}

VIEW_FULL_TEXTS = {
    'articlesummaries': 'View full summary',
    'projectsummaries': 'View full summary',
    'educationalcontent': 'View full course',
    'forum': 'View full post' # not necessary
}

DELETE_TEXTS = {
    'articlesummaries': 'summary',
    'projectsummaries': 'summary',
    'educationalcontent': ['course', 'lecture'],
    'forum': 'post'
}


### paginator
NUMBER_OF_ELEMTS_ON_PAGE = 10


### form
FORM_MIN_YEAR = 0
FORM_MAX_YEAR = 9999

FORM_MAX_IMAGE_SIZE = 7 # in MB


### federated search
SEARCH_FILTERS = {
    'Module': ['Article Summaries', 'Project Summaries', 'Educational Content', 'Forum'],
    'Area': AREA_STRINGS
}
