// save image and caption element
let image = document.getElementById("add-entry").image;
let caption = document.getElementById("add-entry").caption;

// initialise caption-display
if(!document.getElementById("image-clear_id")) {
    caption.style.display = "none";
}

// set onchange-event for image upload
document.getElementById("add-entry").image.onchange = function() {
    if (image.files.length == 0) {
        caption.style.display = "none";
    } else {
        caption.style.display = "inline-block";
    }
};