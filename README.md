# transparency

## Run Transparency as Part of ZLE Platform

Define an environment file with all necessary secret variables ".env" and store it in the respective directory:

    TRANSPARENCY_DB_ADMIN=your_admin
    TRANSPARENCY_DB_PASSWORD=your_password
    TRANSPARENCY_SECRET_KEY=some_secret_key

Get the production docker-compose file from the git repository and copy it to the directory. Either manually, using git or wget. E.g.:

    wget https://gitlab.com/zdin-zle/zle-platform/transparency/-/raw/main/docker-compose.prod.yml
    

Build the containers:

    sudo docker compose -f /home/dev/transparency/docker-compose.prod.yml --env-file /home/dev/transparency/.env  up -d

Update the database structure:

    sudo docker exec transparency_webapp python manage.py migrate

Update the static files:

    sudo docker exec transparency_webapp python manage.py collectstatic --no-input

## Update Transparency on the Server

If the production docker-compose file was changed, get the updated file from the git repository and copy it to the directory (after removing the old one). Either manually, using git or wget. E.g.:

    wget https://gitlab.com/zdin-zle/zle-platform/transparency/-/raw/main/docker-compose.prod.yml

Stop the transparency_webapp container. Delete the old transparency image using:

    sudo docker rm <image-id>    

Build the containers:

    sudo docker compose -f /home/dev/transparency/docker-compose.prod.yml --env-file /home/dev/transparency/.env  up -d

Update the database structure:

    sudo docker exec transparency_webapp python manage.py migrate

Update the static files:

    sudo docker exec transparency_webapp python manage.py collectstatic --no-input

## Run Transparency Locally

To install all necessary objects follow these steps:
1. Download docker from https://www.docker.com
2. Clone repository with `git clone https://gitlab.com/zdin-zle/zle-platform/transparency.git`
3. Download needed the libraries locally from "requirements.txt"

To run the transparency element, create a file ".env" with the following environment variables:

    TRANSPARENCY_DB_ADMIN=your_admin
    TRANSPARENCY_DB_PASSWORD=your_password
    TRANSPARENCY_SECRET_KEY=some_secret_key


Afterwards run

    docker build -t transparency_webapp_image .

    docker compose up -d

    docker exec transparencywebapp python manage.py migrate

You can then access the website in your browser with the url http://localhost:8000/transparency/.


## Development
When updating a model you need to generate a migration file. To do this, run

    cd transparency

    python manage.py makemigrations


in the terminal of your host machine. For development purposes, a dummy sqllite database is defined to allow database checks in makemigrations.

To apply migrations, use the terminal of the transparency_webapp container in docker and run

    python manage.py migrate


## Project structure
### Apps
This project contains the following 4 modules or apps:

- Article Summaries
- Project Summaries
- Educational Content
- Forum

There is a separate folder for each individual app. When you add a new application, you can use the existing files in each folder as a guide. The following files should get further customized:

- `admin.py`: contains the models of that app that should appear in the admin view
- `apps.py`: define the appname that could get accessed in the templates
- `forms.py`: specify your forms of this app in this file
- `models.py`: declare your models with their fields
- `urls.py`: define the used urls in this app and connect them with the respective view
- `views.py`: further define and customize your views of this app

When creating a new app you also have to reserve the url for this app in the file `urls.py`in the folder `transparency`. You also have to add the Config File to the variable `INSTALLED_APPS` of this app to the file `settings.py`in the folder `transparency`.

There is also the folder `main`. This folder contains several general features of the module transparency. Firstly the communication with the federated search takes place in `views.py`. There are two methods defined that send the search results of the transparency app back to the federated search. The respective urls for this are defined in `urls.py`. Secondly the folder `main` contains two files for global variables and classes. These are variables and classes that are used within the whole module transparency and in more than one or all app.

### Templates
The templates of this project could be found in the folder `templates`. The base template for the whole ZLE project and the home template of the transparency module are right in this folder. The templates for the sub-websites (like listing or the form) are found in the folder `templates/app_templates`. Most of the templates are used for all apps. Those templates only used by one app contain the respective app name in their file name.

When creating a new template the structure should be as follows:
    
    {% extends 'base.html' %}
    {% load static %}
    
    {% block content %}
    <div>
        <!-- your content here -->
    </div>
    {% endblock %}

### Static content
The static content of the website is located in the folder `static`. Images of the transparency module could be found in the folder `static/images/transparency`.

CSS and JavaScript content is in the fodler `static/assets/transparency`. The JavaScript files also include the name of the template in which they are used in their file name.


